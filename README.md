# Ember theme for [SNAC2](https://codeberg.org/grunfink/snac2)

colour change to the [white theme by Ворон](https://codeberg.org/voron/snac-style)

dark background, orange links... basically

![Image of Theme](https://codeberg.org/pswilde/snac-ember/raw/branch/master/theme.png)
